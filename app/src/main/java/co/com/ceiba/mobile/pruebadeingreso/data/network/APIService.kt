package co.com.ceiba.mobile.pruebadeingreso.data.network

import co.com.ceiba.mobile.pruebadeingreso.data.model.PostDB
import co.com.ceiba.mobile.pruebadeingreso.data.model.UserDB
import retrofit2.Response
import retrofit2.http.GET

interface APIService {

    @GET("users")
    suspend fun getAllUsers(): Response<List<UserDB>>

    @GET("posts")
    suspend fun getAllPosts(): Response<List<PostDB>>

}