package co.com.ceiba.mobile.pruebadeingreso.ui.view.adapter

import co.com.ceiba.mobile.pruebadeingreso.data.model.UserDB

interface ItemClicListener {

    fun onItemClicked(currentItem: UserDB)
}