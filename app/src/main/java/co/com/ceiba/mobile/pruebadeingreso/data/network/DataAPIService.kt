package co.com.ceiba.mobile.pruebadeingreso.data.network

import co.com.ceiba.mobile.pruebadeingreso.data.model.PostDB
import co.com.ceiba.mobile.pruebadeingreso.data.model.UserDB
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class DataAPIService {
    private val retrofit = RetrofitObject.getRetrofitObject()

    suspend fun getUsersApiData(): List<UserDB> {
        return withContext(Dispatchers.IO) {
            val response = retrofit.create(APIService::class.java).getAllUsers()
            response.body() ?: emptyList()
        }
    }

    suspend fun getPostsApiData(): List<PostDB> {
        return withContext(Dispatchers.IO) {
            val response = retrofit.create(APIService::class.java).getAllPosts()
            response.body() ?: emptyList()
        }
    }
}