package co.com.ceiba.mobile.pruebadeingreso.domain

import android.app.Application
import co.com.ceiba.mobile.pruebadeingreso.data.UserRepository
import co.com.ceiba.mobile.pruebadeingreso.data.database.UserDatabase
import co.com.ceiba.mobile.pruebadeingreso.data.model.UserDB

class GetAllUsersUseCase(application: Application) {

    private val repository: UserRepository

    init {
        val userDao = UserDatabase.getDatabase(application).userDao()
        repository = UserRepository(userDao)
    }

     operator fun invoke():List<UserDB> = repository.getAllUsers()


}