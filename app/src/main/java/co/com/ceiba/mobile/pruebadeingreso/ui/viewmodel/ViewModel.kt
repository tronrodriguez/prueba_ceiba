package co.com.ceiba.mobile.pruebadeingreso.ui.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import co.com.ceiba.mobile.pruebadeingreso.data.model.PostDB

import co.com.ceiba.mobile.pruebadeingreso.data.model.UserDB
import co.com.ceiba.mobile.pruebadeingreso.domain.GetAllUsersUseCase
import co.com.ceiba.mobile.pruebadeingreso.domain.GetDataFromAPI
import co.com.ceiba.mobile.pruebadeingreso.domain.GetPostsByUserIdUseCase
import co.com.ceiba.mobile.pruebadeingreso.utils.Utils.loguer
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ViewModel(application: Application): AndroidViewModel(application) {

    private val app=application

    val listUsersModel=MutableLiveData<List<UserDB>>()
    val isLoading=MutableLiveData<Boolean>()
    val listPostByUserId=MutableLiveData<List<PostDB>>()

    init {
        viewModelScope.launch(Dispatchers.IO) {
            isLoading.postValue(true)
            var getAllUsersUseCase=GetAllUsersUseCase(application)
            val listUsers=getAllUsersUseCase()
            if (!listUsers.isNullOrEmpty()){
                loguer("data already in DB...")
                listUsersModel.postValue(listUsers)
                isLoading.postValue(false)
            }else {
                loguer("retrieving data from API and store in DB...")
                var getApiData=GetDataFromAPI(application)
                getApiData()
                var getAllUsersUseCase=GetAllUsersUseCase(application)
                listUsersModel.postValue(getAllUsersUseCase())
                isLoading.postValue(false)
            }
        }
    }

    fun readPostsByUserId(user: UserDB){
        viewModelScope.launch(Dispatchers.IO){
            isLoading.postValue(true)
            var getPostsByUserId=GetPostsByUserIdUseCase(app,user)
            val listPosts=getPostsByUserId()
            if (!listPosts.isNullOrEmpty()){
                isLoading.postValue(false)
                listPostByUserId.postValue(listPosts)
            }
        }
    }

}