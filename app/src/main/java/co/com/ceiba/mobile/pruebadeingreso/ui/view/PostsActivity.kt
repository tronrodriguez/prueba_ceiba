package co.com.ceiba.mobile.pruebadeingreso.ui.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import co.com.ceiba.mobile.pruebadeingreso.R
import co.com.ceiba.mobile.pruebadeingreso.data.model.PostDB
import co.com.ceiba.mobile.pruebadeingreso.data.model.UserDB
import co.com.ceiba.mobile.pruebadeingreso.ui.view.adapter.ListPostsAdapter
import co.com.ceiba.mobile.pruebadeingreso.utils.Utils.loguer
import kotlinx.android.synthetic.main.activity_post.*


class PostsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_post)
        setUIInfo()
    }

    private fun setUIInfo() {
        val postsList = intent.getSerializableExtra("POSTS_LIST") as List<*>?
        val currentUser=intent.getSerializableExtra("CURRENT_USER") as UserDB
        name.text=currentUser.name
        phone.text=currentUser.phone
        email.text=currentUser.email
        val postRvAdapter=ListPostsAdapter()
        recyclerViewPostsResults.adapter=postRvAdapter
        recyclerViewPostsResults.layoutManager= LinearLayoutManager(this)
        postRvAdapter.setData(postsList as List<PostDB>)
    }
}