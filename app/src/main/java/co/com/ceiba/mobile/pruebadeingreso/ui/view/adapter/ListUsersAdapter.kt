package co.com.ceiba.mobile.pruebadeingreso.ui.view.adapter


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import co.com.ceiba.mobile.pruebadeingreso.R
import co.com.ceiba.mobile.pruebadeingreso.data.model.UserDB
import kotlinx.android.synthetic.main.user_list_item.view.*


class ListUsersAdapter(private val itemClicListener:ItemClicListener):RecyclerView.Adapter<ListUsersAdapter.MyViewHolder>() {

    private var userList= emptyList<UserDB>()


    class MyViewHolder(itemView: View):RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.user_list_item,parent,false))
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var currentItem=userList[position]
        holder.itemView.name.text=currentItem.name
        holder.itemView.phone.text=currentItem.phone
        holder.itemView.email.text=currentItem.email

        holder.itemView.btn_view_post.setOnClickListener {
            itemClicListener.onItemClicked(currentItem)
        }
    }

    override fun getItemCount(): Int {
        return userList.size
    }

    fun setData(users:List<UserDB>){
        this.userList=users
        notifyDataSetChanged()
    }

    fun updateList(list: List<UserDB>) {
        this.userList = list
        notifyDataSetChanged()
    }

}