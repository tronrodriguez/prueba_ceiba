package co.com.ceiba.mobile.pruebadeingreso.ui.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import co.com.ceiba.mobile.pruebadeingreso.R
import co.com.ceiba.mobile.pruebadeingreso.data.model.PostDB
import co.com.ceiba.mobile.pruebadeingreso.data.model.UserDB
import kotlinx.android.synthetic.main.post_list_item.view.*

class ListPostsAdapter: RecyclerView.Adapter<ListPostsAdapter.MyViewHolder>() {

    private var postsList= emptyList<PostDB>()

    class MyViewHolder(itemView: View):RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.post_list_item,parent,false))
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var currentPost=postsList[position]
        holder.itemView.title.text=currentPost.title
        holder.itemView.body.text=currentPost.body

    }

    override fun getItemCount(): Int {
        return postsList.size
    }

    fun setData(posts:List<PostDB>){
        this.postsList=posts
        notifyDataSetChanged()
    }

}