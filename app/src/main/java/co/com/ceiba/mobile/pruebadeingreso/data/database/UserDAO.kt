package co.com.ceiba.mobile.pruebadeingreso.data.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import co.com.ceiba.mobile.pruebadeingreso.data.model.PostDB
import co.com.ceiba.mobile.pruebadeingreso.data.model.UserDB

@Dao
interface UserDAO {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addUser(user: UserDB)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addUsers(listUsers:List<UserDB>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addPosts(listPosts:List<PostDB>)

    @Query("SELECT * FROM users_table ORDER BY userId ASC")
    fun readAllUsers(): List<UserDB>

    @Query("SELECT * FROM posts_table where userId=:id")
    fun readPostsByUserId(id:Int): List<PostDB>
}