package co.com.ceiba.mobile.pruebadeingreso.ui.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import co.com.ceiba.mobile.pruebadeingreso.R
import co.com.ceiba.mobile.pruebadeingreso.data.network.APIService
import co.com.ceiba.mobile.pruebadeingreso.data.network.RetrofitObject
import co.com.ceiba.mobile.pruebadeingreso.data.model.UserDB
import co.com.ceiba.mobile.pruebadeingreso.ui.viewmodel.ViewModel
import co.com.ceiba.mobile.pruebadeingreso.utils.Utils.loguer
import co.com.ceiba.mobile.pruebadeingreso.ui.view.adapter.ItemClicListener
import co.com.ceiba.mobile.pruebadeingreso.ui.view.adapter.ListUsersAdapter
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.w3c.dom.Text
import java.io.Serializable

class MainActivity : AppCompatActivity(), ItemClicListener {
    private lateinit var mUserViewModel: ViewModel
    private lateinit var currentUser: UserDB

    private lateinit var usersRvAdapter: ListUsersAdapter
    private lateinit var listUsuarios: List<UserDB>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mUserViewModel = ViewModelProvider(this).get(ViewModel::class.java)

        usersRvAdapter = ListUsersAdapter(this)
        recyclerViewSearchResults.adapter = usersRvAdapter
        recyclerViewSearchResults.layoutManager = LinearLayoutManager(this)

        mUserViewModel.isLoading.observe(this, Observer { cargando ->
            progress.isVisible = cargando
        })

        mUserViewModel.listUsersModel.observe(this, Observer { listaUsuarios ->
            listUsuarios = listaUsuarios
            usersRvAdapter.setData(listaUsuarios)
        })

        mUserViewModel.listPostByUserId.observe(this, Observer { listaPosts ->
            val intent = Intent(this, PostsActivity::class.java).apply {
                putExtra("POSTS_LIST", listaPosts as Serializable)
                putExtra("CURRENT_USER", currentUser as Serializable)
            }
            startActivity(intent)
        })

        editTextSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                userFilter(s.toString())
            }

        })
    }

    fun userFilter(text: String) {
        var temp = arrayListOf<UserDB>()
        for (user in listUsuarios) {
            var name = user.name
            if (name.lowercase().startsWith(text)) {
                temp.add(user)
            }
        }
        if (temp.isEmpty()){
            Toast.makeText(this,"List is empty",Toast.LENGTH_LONG).show()
        }
        usersRvAdapter.updateList(temp)
    }


    override fun onItemClicked(currentItem: UserDB) {
        currentUser = currentItem
        mUserViewModel.readPostsByUserId(currentItem)
    }
}