package co.com.ceiba.mobile.pruebadeingreso.data

import co.com.ceiba.mobile.pruebadeingreso.data.model.PostDB
import co.com.ceiba.mobile.pruebadeingreso.data.database.UserDAO
import co.com.ceiba.mobile.pruebadeingreso.data.model.UserDB
import co.com.ceiba.mobile.pruebadeingreso.data.network.DataAPIService
import co.com.ceiba.mobile.pruebadeingreso.utils.Utils.loguer

data class UserRepository(private val userDao: UserDAO){

    private val api=DataAPIService()

    suspend fun addUser(user: UserDB){
        userDao.addUser(user)
    }

    suspend fun addAllUsers(listUsers:List<UserDB>){
        userDao.addUsers(listUsers)
    }

    suspend fun addAllPosts(listPosts:List<PostDB>){
        userDao.addPosts(listPosts)
    }

    fun readPostsByUserId(id:Int):List<PostDB>{
        return userDao.readPostsByUserId(id)
    }

    fun getAllUsers():List<UserDB>{
        return userDao.readAllUsers()
    }

    suspend fun getAllUsersApi():List<UserDB>{
        loguer("getAllUsersApi")
        return api.getUsersApiData()
    }

    suspend fun getAllPostsApi():List<PostDB>{
        loguer("getAllPostsApi")
        return api.getPostsApiData()
    }


}
