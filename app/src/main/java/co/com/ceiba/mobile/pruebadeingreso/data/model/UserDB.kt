package co.com.ceiba.mobile.pruebadeingreso.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Entity(tableName = "users_table")
data class UserDB(
    @PrimaryKey(autoGenerate = false)
    @SerializedName("id") val userId: Int,
    @SerializedName("name") val name: String,
    @SerializedName("username") val username: String,
    @SerializedName("email") val email: String,
    @SerializedName("phone") val phone: String,
    @SerializedName("website") val website: String,
):Serializable
