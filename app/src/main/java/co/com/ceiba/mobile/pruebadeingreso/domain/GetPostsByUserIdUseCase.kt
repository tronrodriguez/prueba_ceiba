package co.com.ceiba.mobile.pruebadeingreso.domain

import android.app.Application
import co.com.ceiba.mobile.pruebadeingreso.data.UserRepository
import co.com.ceiba.mobile.pruebadeingreso.data.database.UserDatabase
import co.com.ceiba.mobile.pruebadeingreso.data.model.PostDB
import co.com.ceiba.mobile.pruebadeingreso.data.model.UserDB

class GetPostsByUserIdUseCase(application: Application, user: UserDB) {
    private val repository: UserRepository
    private val id = user.userId

    init {
        val userDao = UserDatabase.getDatabase(application).userDao()
        repository = UserRepository(userDao)
    }

    suspend operator fun invoke(): List<PostDB>? = repository.readPostsByUserId(id)
}