package co.com.ceiba.mobile.pruebadeingreso.domain

import android.app.Application
import co.com.ceiba.mobile.pruebadeingreso.data.UserRepository
import co.com.ceiba.mobile.pruebadeingreso.data.database.UserDatabase

class GetDataFromAPI(application: Application) {
    private val repository: UserRepository

    init {
        val userDao = UserDatabase.getDatabase(application).userDao()
        repository = UserRepository(userDao)
    }

    suspend operator fun invoke(){
        repository.addAllUsers(repository.getAllUsersApi())
        repository.addAllPosts(repository.getAllPostsApi())
    }
}