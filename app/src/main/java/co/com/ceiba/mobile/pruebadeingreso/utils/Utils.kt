package co.com.ceiba.mobile.pruebadeingreso.utils

import android.util.Log
import co.com.ceiba.mobile.pruebadeingreso.utils.Constants.TAG

object Utils {

    fun loguer(str:String){
        Log.d(TAG,str)
    }
}